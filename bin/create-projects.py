#!/usr/bin/python

import yaml
import os

# Read user roles and projects
with open("data/roles-projects-map.yml") as f:
    roles_projects_map = yaml.safe_load(f)

# Assign cluster administrators
print "Cluster administrators:"
for user in roles_projects_map["cluster-admin"]:
    print "  {}".format(user["user"])
    os.system("oc adm policy add-cluster-role-to-user {} {}".format("cluster-admin", user["user"])) 

# Assign cluster auditors
print "Cluster auditors:"
for user in roles_projects_map["cluster-reader"]:
    print "  {}".format(user["user"])
    os.system("oc adm policy add-cluster-role-to-user {} {}".format("cluster-reader", user["user"])) 

# Create projects and assign roles to users
print "Projects:"
for project in roles_projects_map["projects"]:
    print "  {}".format(project["name"])
    os.system("oc new-project {}".format(project["name"]))
    if "users" in project:
        print "    Users:"
        for user in project["users"]:
            print "      {} : {}".format(user["name"], user["role"])
            os.system("oc adm policy add-role-to-user {} {} -n {}".format(user["role"], user["name"], project["name"]))
