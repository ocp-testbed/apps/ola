openshift.withCluster() {
  // Obtain namespace and application name
  env.NAMESPACE = openshift.project()
  env.APP_NAME = "${env.JOB_NAME}".replaceAll(/-?pipeline-?/, '').replaceAll(/-?${env.NAMESPACE}-?/, '').replaceAll("/", '')
  echo "Starting Pipeline for ${env.APP_NAME}..."

  // Obtain application namespaces
  def projectBase = "${env.NAMESPACE}".replaceAll(/-build/, '')
  env.BUILD = "${projectBase}-build"
  env.DEV = "${projectBase}-dev"
  env.QA = "${projectBase}-qa"
  env.PROD = "${projectBase}-prod"

  // Testing routes
  env.BASE_FQDN = "apps.ocp-testbed.com"
  env.DEV_ROUTE = "${env.APP_NAME}-${env.DEV}.${env.BASE_FQDN}"

  // Quay
  env.QUAY_URL = "quay.io"
  env.QUAY_REPO = "pdred/${env.APP_NAME}"
  env.QUAY_SIGN_TEMPLATE = "https://raw.githubusercontent.com/redhat-cop/image-scanning-signing-service/master/examples/image-signing-request-template.yml"
}

pipeline {
  // Use Jenkins Maven slave
  // Jenkins will dynamically provision this as OpenShift Pod
  // All the stages and steps of this Pipeline will be executed on this Pod
  // After Pipeline completes the Pod is killed so every run will have clean
  // workspace
  agent {
    label 'maven'
  }

  // Pipeline Stages start here
  // Requires at least one stage
  stages {
    // Checkout source code
    // This is required as Pipeline code is originally checkedout to
    // Jenkins Master but this will also pull this same code to this slave
    stage('Code Checkout') {
      steps {
        checkout scm
      }
    }

    // Run Maven build, skipping tests
    stage('Code Build') {
      steps {
        sh "mvn clean package -DskipTests=true"
      }
    }

    // Run Maven unit tests
    stage('Unit Test') {
      steps {
        sh "mvn test"
      }
    }

    stage('Sonar Scan') {
      steps {
        sleep 1
      }
    }
    
    stage('Application Component Scan') {
      steps {
        echo "stub for Application Component Scan"
        sleep 1
      }
    }

    // Build Container Image using the artifacts produced in previous stages
    stage('Build Container Image'){
      steps {
        // Copy the resulting artifacts into common directory
        sh """
          ls target/*
          rm -rf oc-build && mkdir -p oc-build/deployments
          for t in \$(echo "jar;war;ear" | tr ";" "\\n"); do
            cp -rfv ./target/*.\$t oc-build/deployments/ 2> /dev/null || echo "No \$t files"
          done
        """

        // Build container image using local Openshift cluster
        // Giving all the artifacts to OpenShift Binary Build
        // This places your artifacts into right location inside your S2I image
        // if the S2I image supports it.
        script {
          openshift.withCluster() {
            openshift.withProject("${env.BUILD}") {
              bc = openshift.selector("bc/${env.APP_NAME}").object()
              image = bc.spec.output.to.name
              tag = image.replaceAll("^.+?:", "")


              env.IMAGE_NAME = bc.spec.output.to.name.replaceAll("-\\d{1,}\$", "-${env.BUILD_NUMBER}")
              echo "==== Image Name = ${env.IMAGE_NAME}"
              env.IMAGE_TAG = bc.spec.output.to.name.replaceAll("^.+?:", "")
              echo "==== Image Tag = ${env.IMAGE_TAG}"

              echo "==== Building container image"
              build = openshift.startBuild("${env.APP_NAME}", "--from-dir=oc-build")
              echo "=== Got Build"

              timeout(10) {
                build.untilEach {
                  def phase = it.object().status.phase
                  echo "Build Status: ${phase}"

                  if (phase == "Complete") {
                    return true
                  }
                  else if (phase == "Failed") {
                    currentBuild.result = "FAILURE"
                    buildErrorLog = it.logs().actions[0].err
                    return true
                  }
                  else {
                    return false
                  }
                }
              }

              if (currentBuild.result == 'FAILURE') {
                error(buildErrorLog)
                return
              }
            }
          }
        }
      }
    }

//////////////////////////////

    stage('Push Image to External Registry') {
      steps {
        sleep 1
      }
    }
    
    stage('Image Scan') {
      steps {
        script {
          tag = image.replaceAll("^.+?:","")
          tagInfo = httpRequest ignoreSslErrors:true, url:"http://${env.QUAY_URL}/api/v1/repository/${env.QUAY_REPO}/tag/${tag}/images"
          tagInfo = readJSON text: tagInfo.content
          index_max = -1
          for( imageRef in tagInfo.images ) {
            if( imageRef.sort_index > index_max ) {
              imageId = imageRef.id
              index_max = imageRef.sort_index
            }
          }

          timeout(time: 5, unit: 'MINUTES') {
            waitUntil() {
              vulns = httpRequest ignoreSslErrors:true, url:"https://${env.QUAY_URL}/api/v1/repository/${env.QUAY_REPO}/image/${imageId}/security?vulnerabilities=true"
              vulns = readJSON text: vulns.content  
              if(vulns.status != "scanned") {
                return false
              }

              low=[]
              medium=[]
              high=[]
              critical=[]

              for ( rpm in vulns.data.Layer.Features ) {
                vulnList = rpm.Vulnerabilities
                if(vulnList != null && vulnList.size() != 0) {
                  i = 0;
                  for(vuln in vulnList) {
                    switch(vuln.Severity) {
                      case "Low":
                        low.add(vuln)
                        break
                      case "Medium":
                        medium.add(vuln)
                        break
                      case "High":
                        high.add(vuln)
                        break
                      case "Critical":
                        critical.add(vuln)
                        break
                      default:
                        echo "Should never be here"
                        currentBuild.result = "FAILURE"
                        break
                    }
                  }
                }
              }
                         
              return true
            }
          }

          if(critical.size() > 0 || high.size() > 0) {
            input "Image has ${critical.size()} critical vulnerabilities and ${high.size()} high vulnerabilities. Please check https://${env.QUAY_URL}/repository/${env.QUAY_REPO}/image/${imageId}?tab=vulnerabilities.  Would you like to proceed anyway?"
            currentBuild.result = "UNSTABLE"
          }
        }
      }
    }

//    stage('Sign Image') {
//      steps {
//        script {
//          openshift.withCluster() {
//            sh " oc import-image ${APP_NAME}:${tag} --from=${env.QUAY_URL}/${env.QUAY_REPO}:${tag}"
//            obj = "${APP_NAME}-${env.BUILD_NUMBER}"
//            created = openshift.create(openshift.process(template, "-p IMAGE_SIGNING_REQUEST_NAME=${obj} -p IMAGE_STREAM_TAG=${APP_NAME}:${tag}"))
//
//            imagesigningrequest = created.narrow('imagesigningrequest').name();
//
//            echo "ImageSigningRequest ${imagesigningrequest.split('/')[1]} Created"
//
//            timeout(time: 5, unit: 'MINUTES') {
//
//            waitUntil() {
//              def isr = openshift.selector("${imagesigningrequest}")
//
//              if(isr.object().status) {
//
//                def phase = isr.object().status.phase
//
//                  if(phase == "Failed") {
//                    echo "Signing Action Failed: ${isr.object().status.message}"
//                    currentBuild.result = "FAILURE"
//                    return true
//                  }
//                  else if(phase == "Completed") {
//                    env.SIGNED_IMAGE = isr.object().status.signedImage
//                    echo "Signing Action Completed. Signed Image: ${SIGNED_IMAGE}"
//                    return true
//                  }
//              }
//              else {
//                echo "Status is null"
//              }
//
//              return false
//              }
//            }  
//          } 
//        }
//      }
//    }

    stage('Dev Deploy') {
      steps {
        script {
          openshift.withCluster() {
            openshift.tag("${env.APP_NAME}:${tag}", "${env.DEV}/${env.APP_NAME}:dev")
          }
        }
      }
    }

//    stage ('Verify Deploy to Dev') {
//      steps {
//        script {
//          timeout(time: 1, unit: 'MINUTES') {
//            openshift.withCluster() {
//              openshift.withProject("${env.DEV}") {
//                def dcObj = openshift.selector('dc', env.APP_NAME).object()
//                def podSelector = openshift.selector('pod', [deployment: "${APP_NAME}-${dcObj.status.latestVersion}"])
//                podSelector.untilEach {
//                  echo "pod: ${it.name()}"
//                  return it.object().status.containerStatuses[0].ready
//                }
//              }
//              echo "Verified deployment"
//              currentBuild.result = "Verified DEV deployment: " + current.result
//            }
//          }
//        }
//      }
//    }

    stage('Dev Smoke Test') {
      steps {
        script {
          sleep 120
          timeout(time: 30, unit: 'SECONDS') {
            waitUntil() {
              response = httpRequest ignoreSslErrors:true, url:"http://${env.DEV_ROUTE}"
              if( response.status == 200 ){
                return true
              }
              return false
            }
          }
        }
      }
    }

    stage('QA Deploy') {
      steps {
        script {
          openshift.withCluster() {
            openshift.tag("${APP_NAME}:${tag}", "${env.QA}/${APP_NAME}:qa")
          }
        }
      }
    }

    stage('Prod Deploy') {
      steps {
        script {
          input "Deploy to Prod"
          openshift.withCluster() {
            openshift.tag("${APP_NAME}:${tag}", "${env.PROD}/${APP_NAME}:prod")
          }
        }
      }
    }

  }
}
